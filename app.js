const { response } = require('express')
const express = require('express')
const app = express()
const knex = require('knex')
const config = require("./knexfile.js")
const database = knex(config.development)
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser')

const SECRET_TOKEN = "67t47u3frochvjdfsl"

app.set('view engine', 'ejs')

//Middleware
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())


const authorization = (req, res, next) => {
    console.log('COOKIES', req.cookies)
    const token = req.cookies.__auth__;
    console.log("token in auth", token)
    if (!token) {
        return res.sendStatus(403);
    }
    try {
        const user = jwt.verify(token, SECRET_TOKEN)
        req.user = user.email;
        // console.log(checkData)
        return next();
    } catch {
        return res.sendStatus(403);
    }
};


// routes
app.get("/", (req, res) => {
    res.send("Hello")
})


app.get("/alter_password", (req, res) => {
    res.render("alter_passwor")
})

app.get("/admin", authorization, (req, res) => {
    res.render("user_list")
})


// create account
app.get("/sign-up", (req, res) => {
    res.render("sign_up")
})



app.post("/sign-up", async function (req, res) {
    const users_mail = await database
        .select("*")
        .from("users")
        .where({ email: req.body.spectre_mail })

    if (users_mail.length === 0 && req.body.spectre_password === req.body.spectre_confirm_password) {
        const email = req.body.spectre_mail
        const password = req.body.spectre_password
        const saltRounds = 8
        const hash = bcrypt.hashSync(password, saltRounds)
        const new_user = await database
            .into("users")
            .insert({ email: email, name: "null", password: hash })
        console.log(new_user)
        res.render("sign_in")
    } else {
        res.render("sign_up_error")
    }
})

// login
app.get("/login", (req, res) => {
    res.render("sign_in")
})

app.post("/login", async function (req, res) {

    const usersList = await database
        .select("*")
        .from("users")
        .where({ email: req.body.spectre_mail })
    console.log(usersList)
    if (usersList.length > 0) {
        const user = usersList[0]
        const isPasswordCorrect = bcrypt.compareSync(req.body.spectre_password, user.password)
        const token = jwt.sign({ email: user.email }, SECRET_TOKEN)
        console.log("token", token)
        return res.cookie("__auth__", token, {
            httpOnly: true,
            secure: false
        })
    } else {
        res.render("sign_in_error")
    }
})



app.listen(3002)